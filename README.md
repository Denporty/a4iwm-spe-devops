# A4IWM-Spe-DevOps
This group is composed of Victor GAUBIN, Yanny OUZID and Victor CABOT.


## Context

This repository is a student project of DevOps (Docker and CI with GitLab). In that project we have create 5 jobs with GitLab CI (build, lint, test, notification and deploy).

## Stack

<ul>
    <li>Laravel <a href="https://laravel.com/docs/8.x">documentation link</a></li>
    <li>Vue <a href="https://vuejs.org/v2/guide/">documentation link</a></li>
    <li>Node <a href="https://hub.docker.com/_/node/">documentation link</a></li>
    <li>MySQL <a href="https://hub.docker.com/_/mysql">documentation link</a></li>
    <li>Heroku <a href="https://devcenter.heroku.com/">documentation link</a></li>
</ul>
