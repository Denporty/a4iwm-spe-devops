<!DOCTYPE html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Laravel with Inertia</title>
		<?php echo vite_tags(e("default")); ?>
	</head>
	<body class="antialiased bg-gray-900">
		<?php if (!isset($__inertiaSsr)) { $__inertiaSsr = app(\Inertia\Ssr\Gateway::class)->dispatch($page); }  if ($__inertiaSsr instanceof \Inertia\Ssr\Response) { echo $__inertiaSsr->body; } else { ?><div id="app" data-page="<?php echo e(json_encode($page)); ?>"></div><?php } ?>
	</body>
</html>
<?php /**PATH /Users/aono/Documents/web/etc/Cours/Spe-Devops/a4iwm-spe-devops/resources/views/app.blade.php ENDPATH**/ ?>